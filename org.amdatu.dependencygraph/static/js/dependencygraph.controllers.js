var controllers = angular.module('dependencyControllers', []);
controllers.controller('DependencyCtrl', function DependencyCtrl($scope, $http) {
 	$http.get("/rest/dependencies").success(function(data) {
 		$scope.data = data;
 		$scope.draw();
 	});
 	
 	$scope.draw = function() {
 		$("svg").remove();
 		
 		var w = 2000, h = 1200;
		
		var vis = d3.select("graph").append("svg:svg").attr("width", w).attr("height", h);

		var nodes = [];
		var links = [];
		angular.forEach($scope.data, function(node) {
			if($scope.filter == undefined || node.name.indexOf($scope.filter) != -1 || hasDependencyOnFilter(node)) {
				nodes.push(node);
			}
		});
		
		function hasDependencyOnFilter(node) {
			var result = false;
			
			angular.forEach(node.dependencies, function(dependency) {
				if(dependency.indexOf($scope.filter) != -1) {
					result = true;
					return false;
				}
			});
			
			return result;
		}

		angular.forEach(nodes, function(node) {
			angular.forEach(node.dependencies, function(dependency) {
				angular.forEach(nodes, function(otherNode) {
					if(otherNode.name == dependency) {
						links.push({
							source: node,
							target: otherNode,
							weight: 1
						});
					}
				});
			});
			
		});

		var force = d3.layout.force()
		    .size([w, h])
		    .nodes(nodes)
    		.distance(200)
    		.gravity(.05)
    		.links(links)
    		.charge(-600).start();
		
		var svg = d3.select("body").append("svg")
		    .attr("width", w)
		    .attr("height", h);
		    
		svg.append("svg:defs").selectAll("marker")
	    .data(["suit", "licensing", "resolved"])
	 	.enter().append("svg:marker")
	    .attr("id", String)
	    .attr("viewBox", "0 -5 10 10")
	    .attr("refX", 15)
	    .attr("refY", -1.5)
	    .attr("markerWidth", 6)
	    .attr("markerHeight", 6)
	    .attr("orient", "auto")
	    .append("svg:path")
	    .attr("d", "M0,-5L10,0L0,5");
	
	var path = svg.append("svg:g").selectAll("path")
	    .data(force.links())
	  .enter().append("svg:path")
	    .attr("class", function(d) { return "link " + d.type; })
	    .attr("marker-end", function(d) { return "url(#licensing)"; });    
		 	
		 	
	 	var link = svg.selectAll(".link")
	      .data(links)
	      .enter().append("line")
	      .attr("class", "link");
	
	  	var node = svg.selectAll(".node")
	      .data(nodes)
	      .enter().append("text")
	      .attr("class", "node")
	      .call(force.drag).text(function(d) { 
	      	if(d.name.indexOf(".rest") != -1) {
	      		return d.name;
	      	} else {
	      		return d.name.substring(d.name.lastIndexOf(".") + 1)	
	      	}
      	  	 
      	  });;
	      
	      
	    force.on("tick", function() {
	    	 path.attr("d", function(d) {
			    var dx = d.target.x - d.source.x,
			        dy = d.target.y - d.source.y,
			        dr = Math.sqrt(dx * dx + dy * dy);
			    return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
			  });

  	    	
    		link.attr("x1", function(d) { return d.source.x; })
        	.attr("y1", function(d) { return d.source.y; })
        	.attr("x2", function(d) { return d.target.x; })
        	.attr("y2", function(d) { return d.target.y; });

    	node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  });
	  
	  
    }
});
